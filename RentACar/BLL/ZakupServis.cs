﻿using RentACar.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.BLL
{
    public class ZakupServis : IDisposable
    {
        RentACarEntities db = new RentACarEntities();

        /// <summary>
        /// Dohvaća filijale prema gradu u kome se filijala nalazi.
        /// </summary>
        /// <param name="GradID"></param>
        /// <returns></returns>
        public IEnumerable<Filijala> DohvatiFilijalePoGradu(Guid GradID)
        {
            return db.Filijalas.Where(x => x.GradID == GradID);
        }

        /// <summary>
        /// Dohvaća gradove, sortirane prema nazivu grada.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Grad> DohvatiGradove()
        {
            return db.Grads.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća vozila, sortirana prema nazivu proizvođača.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Vozilo> DohvatiVozila()
        {
            return db.Voziloes.OrderBy(x => x.ModelVozila.Proizvodjac.Naziv);
        }

        /// <summary>
        /// Dohvaća vozilo prema filijali kojoj pripada.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public IEnumerable<Vozilo> DohvatiVozilaPoFilijali(Guid ID)
        {
            return db.Voziloes.Where(x => x.FilijalaID == ID).OrderBy(x => x.ModelVozila.Proizvodjac.Naziv); ;
        }

        /// <summary>
        /// Dohvaća vozilo prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Vozilo DohvatiVozilo(Guid ID)
        {
            return db.Voziloes.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća klijenta prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Klijent DohvatiKlijenta(Guid ID)
        {
            return db.Klijents.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća filijalu prema njenom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Filijala DohvatiFilijalu(Guid ID)
        {
            return db.Filijalas.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća sve filijale, sortirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Filijala> DohvatiFilijale()
        {
            return db.Filijalas.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća sve zakupe, sortirane prema datumu zakupa.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Zakup> DohvatiZakupe()
        {
            return db.Zakups.OrderBy(x => x.DatumZakupa);
        }

        /// <summary>
        /// Dohvaća zakupe koji imaju status "u procesu", i sortira ih prena datumu zakupa.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Zakup> DohvatiZakupeUProcesu()
        {
            return db.Zakups.Where(x => x.StatusID == 2).OrderBy(x => x.DatumZakupa);
        }

        /// <summary>
        /// Dohvaća ukupan broj zakupa prema mjesecu u kom je zakup napravljen.
        /// </summary>
        /// <returns></returns>
        public List<int> DohvatiZakupePoMjesecima()
        {
            var zakupi = new List<int>();

            for (int i = 1; i < 13; i++)
            {
                zakupi.Add(db.Zakups.Where(x => x.DatumZakupa.Value.Month == i && x.DatumZakupa.Value.Year ==
                    DateTime.Now.Year).Count());
            }

            return zakupi;
        }

        /// <summary>
        /// Dohvaća zakup prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Zakup DohvatiZakup(Guid ID)
        {
            return db.Zakups.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Čuva zakup.
        /// </summary>
        /// <param name="zakup"></param>
        public void Sacuvaj(Zakup zakup)
        {
            db.Zakups.Add(zakup);
            db.SaveChanges();
        }

        /// <summary>
        /// Briše zakup.
        /// </summary>
        /// <param name="zakup"></param>
        public void Obrisi(Zakup zakup)
        {
            db.Zakups.Remove(zakup);
            db.SaveChanges();
        }

        /// <summary>
        /// Ažurira zakup.
        /// </summary>
        /// <param name="zakup"></param>
        public void Azuriraj(Zakup zakup)
        {
            var model = db.Zakups.FirstOrDefault(x => x.ID == zakup.ID);

            model.KlijentID = zakup.KlijentID;
            model.DatumPovratka = zakup.DatumPovratka;
            model.DatumZakupa = zakup.DatumZakupa;
            model.VoziloID = zakup.VoziloID;
            model.Komentar = zakup.Komentar;
            model.Ukupno = zakup.Ukupno;
            model.StatusID = 2;
            db.SaveChanges();
        }

        /// <summary>
        /// Mijenja status zakupa.
        /// </summary>
        /// <param name="ZakupID"></param>
        public void PromjeniStatus(Guid ZakupID)
        {
            var model = db.Zakups.FirstOrDefault(x => x.ID == ZakupID);

            model.StatusID = 3;

            db.SaveChanges();
        }

        /// <summary>
        /// Provjerava dostupnost zadanog vozila, prema željenom periodu.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="DatumOd"></param>
        /// <param name="DatumDo"></param>
        /// <returns></returns>
        public bool DostupnostVozila(Guid ID, DateTime DatumOd, DateTime DatumDo)
        {
            var zakupi = db.Zakups.Where(x => x.VoziloID == ID && x.StatusID != 3 
                && ((x.DatumZakupa.Value < DatumOd && DatumOd < x.DatumPovratka.Value)
                || (x.DatumZakupa.Value < DatumDo && DatumDo <= x.DatumPovratka.Value)
                || (DatumOd < x.DatumZakupa.Value && x.DatumZakupa.Value < DatumDo)
                || (DatumOd < x.DatumPovratka.Value &&
                    x.DatumPovratka.Value <= DatumDo)));

            if (!zakupi.Any())
                return true;
            else
                return false;
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}