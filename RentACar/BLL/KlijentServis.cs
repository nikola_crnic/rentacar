﻿using RentACar.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.BLL
{
    public class KlijentServis : IDisposable
    {
        RentACarEntities db = new RentACarEntities();

        /// <summary>
        /// Dohvaća zakupe za traženog klijenta.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public IEnumerable<Zakup> DohvatiZakupePoKlijentu(Guid ID)
        {
            var zakupi = db.Zakups.Where(x => x.KlijentID == ID);
            return db.Zakups.Where(x => x.KlijentID == ID);
        }

        /// <summary>
        /// Dohvaća klijente, sortiran prema imenu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Klijent> DohvatiKlijente()
        {
            return db.Klijents.OrderBy(x => x.Ime);
        }

        /// <summary>
        /// Dohvaća klijenta prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Klijent DohvatiKlijenta(Guid ID)
        {
            return db.Klijents.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća klijenta prema njegovom email-u.
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public Klijent DohvatiKlijentaPoEmailu(string Email)
        {
            return db.Klijents.FirstOrDefault(x => x.Email == Email);
        }

        /// <summary>
        /// Čuva klijenta.
        /// </summary>
        /// <param name="klijent"></param>
        public void Sacuvaj(Klijent klijent)
        {
            db.Klijents.Add(klijent);
            db.SaveChanges();
        }

        /// <summary>
        /// Briše klijenta.
        /// </summary>
        /// <param name="klijent"></param>
        public void Obrisi(Klijent klijent)
        {
            db.Klijents.Remove(klijent);
            db.SaveChanges();
        }

        /// <summary>
        /// Ažurira klijenta
        /// </summary>
        /// <param name="klijent"></param>
        public void Azuriraj(Klijent klijent)
        {
            var model = db.Klijents.FirstOrDefault(x => x.ID == klijent.ID);

            model.BrojLK = klijent.BrojLK;
            model.BrojVD = klijent.BrojVD;
            model.Ime = klijent.Ime;
            model.Prezime = klijent.Prezime;
            model.JMBG = klijent.JMBG;

            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}