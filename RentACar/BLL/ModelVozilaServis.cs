﻿using RentACar.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.BLL
{
    public class ModelVozilaServis : IDisposable
    {
        RentACarEntities db = new RentACarEntities();

        /// <summary>
        /// Dohvaća proizvođače, sortirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Proizvodjac> DohvatiProizvodjace()
        {
            return db.Proizvodjacs.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća tipove goriva, sortirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TipGoriva> DohvatiTipoveGoriva()
        {
            return db.TipGorivas.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća tipove vozila, sortirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TipVozila> DohvatiTipoveVozila()
        {
            return db.TipVozilas.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća modele vozila, soritirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ModelVozila> DohvatiModeleVozila()
        {
            return db.ModelVozilas.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća model vozila prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public ModelVozila DohvatiModelVozila(Guid ID)
        {
            return db.ModelVozilas.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća model vozila prema nazivu.
        /// </summary>
        /// <param name="Naziv"></param>
        /// <returns></returns>
        public ModelVozila DohvatiModelVozilaPoNazivu(string Naziv)
        {
            return db.ModelVozilas.FirstOrDefault(x => x.Naziv.Equals(Naziv, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Čuva model vozila.
        /// </summary>
        /// <param name="modelVozila"></param>
        public void Sacuvaj(ModelVozila modelVozila)
        {
            db.ModelVozilas.Add(modelVozila);
            db.SaveChanges();
        }

        /// <summary>
        /// Briše model vozila.
        /// </summary>
        /// <param name="modelVozila"></param>
        public void Obrisi(ModelVozila modelVozila)
        {
            db.ModelVozilas.Remove(modelVozila);
            db.SaveChanges();
        }

        /// <summary>
        /// Ažurira model vozila.
        /// </summary>
        /// <param name="modelVozila"></param>
        public void Azuriraj(ModelVozila modelVozila)
        {
            var model = db.ModelVozilas.FirstOrDefault(x => x.ID == modelVozila.ID);

            model.Automatik = modelVozila.Automatik;
            model.Naziv = modelVozila.Naziv;
            model.ProizvodjacID = modelVozila.ProizvodjacID;
            model.TipGorivaID = modelVozila.TipGorivaID;
            model.TipVozilaID = modelVozila.TipVozilaID;
            model.BrojOsoba = modelVozila.BrojOsoba;

            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}