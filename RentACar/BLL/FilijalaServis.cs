﻿using RentACar.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.BLL
{
    public class FilijalaServis : IDisposable
    {
        RentACarEntities db = new RentACarEntities();

        /// <summary>
        /// Dohvaća sve gradove, sortirane prema nazivu.
        /// </summary>
        public IEnumerable<Grad> DohvatiGradove()
        {
            return db.Grads.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća grad prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        public Grad DohvatiGrad(Guid ID)
        {
            return db.Grads.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća sve filijale, sortirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Filijala> DohvatiFilijale()
        {
            return db.Filijalas.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća filijalu prema njenom nazivu.
        /// </summary>
        /// <param name="Naziv"></param>
        /// <returns></returns>
        public Filijala DohvatiFilijaluPoNazivu(string Naziv)
        {
            return db.Filijalas.FirstOrDefault(x => x.Naziv.Equals(Naziv, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Dohvaća filijalu prema njenom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Filijala DohvatiFilijalu(Guid ID)
        {
            return db.Filijalas.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća promet ostvaren u filijalama, sortiran u opadajućem obliku.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Models.ZakupFilijala> DohvatiPrometPoFilijalama()
        {
            return db.Zakups.GroupBy(x => x.FilijalaID).OrderByDescending(y => y.Count())
                .Select(y => new Models.ZakupFilijala
                {
                    ID = y.Select(z => z.Filijala.ID).FirstOrDefault(),
                    Adresa = y.Select(z => z.Filijala.Adresa).FirstOrDefault(),
                    Naziv = y.Select(z => z.Filijala.Naziv).FirstOrDefault(),
                    Grad = y.Select(z => z.Filijala.Grad.Naziv).FirstOrDefault(),
                    Promet = y.Count()
                });
        }

        /// <summary>
        /// Čuva filijalu.
        /// </summary>
        /// <param name="filijala"></param>
        public void Sacuvaj(Filijala filijala)
        {
            db.Filijalas.Add(filijala);
            db.SaveChanges();
        }

        /// <summary>
        /// Briše filijalu.
        /// </summary>
        /// <param name="filijala"></param>
        public void Obrisi(Filijala filijala)
        {
            db.Filijalas.Remove(filijala);
            db.SaveChanges();
        }

        /// <summary>
        /// Ažurira filijalu.
        /// </summary>
        /// <param name="filijala"></param>
        public void Azuriraj(Filijala filijala)
        {
            var model = db.Filijalas.FirstOrDefault(x => x.ID == filijala.ID);

            model.Adresa = filijala.Adresa;
            model.BrojTelefona = filijala.BrojTelefona;
            model.GradID = filijala.GradID;

            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}