﻿using RentACar.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.BLL
{
    public class ProizvodjacServis : IDisposable
    {
        RentACarEntities db = new RentACarEntities();

        /// <summary>
        /// Dohvaća proizvođače, sortirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Proizvodjac> DohvatiProizvodjace()
        {
            return db.Proizvodjacs.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća proizvođača prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Proizvodjac DohvatiProizvodjaca(Guid ID)
        {
            return db.Proizvodjacs.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća proizvođača prema njegovom nazivu.
        /// </summary>
        /// <param name="Naziv"></param>
        /// <returns></returns>
        public Proizvodjac DohvatiProizvodjacaPoNazivu(string Naziv)
        {
            return db.Proizvodjacs.FirstOrDefault(x => x.Naziv.Equals(Naziv, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Čuva proizvođača.
        /// </summary>
        /// <param name="proizvodjac"></param>
        public void Sacuvaj(Proizvodjac proizvodjac)
        {
            db.Proizvodjacs.Add(proizvodjac);
            db.SaveChanges();
        }

        /// <summary>
        /// Briše proizvođača.
        /// </summary>
        /// <param name="proizvodjac"></param>
        public void Obrisi(Proizvodjac proizvodjac)
        {
            db.Proizvodjacs.Remove(proizvodjac);
            db.SaveChanges();
        }

        /// <summary>
        /// Ažurira proizvođača.
        /// </summary>
        /// <param name="proizvodjac"></param>
        public void Azuriraj(Proizvodjac proizvodjac)
        {
            var model = db.Proizvodjacs.FirstOrDefault(x => x.ID == proizvodjac.ID);

            model.Logo = proizvodjac.Logo;

            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}