﻿using RentACar.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.BLL
{
    public class GradServis : IDisposable
    {
        RentACarEntities db = new RentACarEntities();

        /// <summary>
        /// Dohvaća sve gradove, sortirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Grad> DohvatiGradove()
        {
            return db.Grads.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća grad prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Grad DohvatiGrad(Guid ID)
        {
            return db.Grads.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća grad prema njegovom nazivu.
        /// </summary>
        /// <param name="Naziv"></param>
        /// <returns></returns>
        public Grad DohvatiGradPoNazivu(string Naziv)
        {
            return db.Grads.FirstOrDefault(x => x.Naziv.Equals(Naziv, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Čuva grad.
        /// </summary>
        /// <param name="grad"></param>
        public void Sacuvaj(Grad grad)
        {
            db.Grads.Add(grad);
            db.SaveChanges();
        }

        /// <summary>
        /// Briše grad.
        /// </summary>
        /// <param name="grad"></param>
        public void Obrisi(Grad grad)
        {
            db.Grads.Remove(grad);
            db.SaveChanges();
        }

        /// <summary>
        /// Azuriraj grad.
        /// </summary>
        /// <param name="grad"></param>
        public void Azuriraj(Grad grad)
        {
            var model = db.Grads.FirstOrDefault(x => x.ID == grad.ID);

            model.PozivniBroj = grad.PozivniBroj;

            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}