﻿using RentACar.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.BLL
{
    public class KorisnikServis : IDisposable
    {
        RentACarEntities db = new RentACarEntities();

        /// <summary>
        /// Dohvaća korisnika prema njegovom email-u.
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public Korisnik DohvatiKorisnika(string Email)
        {
            return db.Korisniks.FirstOrDefault(x => x.Email == Email);
        }

        /// <summary>
        /// Čuva korisnika.
        /// </summary>
        /// <param name="korisnik"></param>
        public void Sacuvaj(Korisnik korisnik)
        {
            db.Korisniks.Add(korisnik);
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}