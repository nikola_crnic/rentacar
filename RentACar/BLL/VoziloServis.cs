﻿using RentACar.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.BLL
{
    public class VoziloServis : IDisposable
    {
        RentACarEntities db = new RentACarEntities();

        /// <summary>
        /// Dohvaća filijale, sortirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Filijala> DohvatiFilijale()
        {
            return db.Filijalas.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća modele vozila, sortirane prema nazivu.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ModelVozila> DohvatiModeleVozila()
        {
            return db.ModelVozilas.OrderBy(x => x.Naziv);
        }

        /// <summary>
        /// Dohvaća vozila, sortirane prema nazivu proizvođača.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Vozilo> DohvatiVozila()
        {
            return db.Voziloes.OrderBy(x => x.ModelVozila.Proizvodjac.Naziv);
        }

        /// <summary>
        /// Dohvaća vozilo prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Vozilo DohvatiVozilo(Guid ID)
        {
            return db.Voziloes.FirstOrDefault(x => x.ID == ID);
        }

        /// <summary>
        /// Dohvaća listu najčešće iznajmljivanih vozila.
        /// </summary>
        /// <param name="Broj"></param>
        /// <returns></returns>
        public IEnumerable<Models.ZakupVozilo> DohvatiTopVozila(int Broj)
        {
            return db.Zakups.GroupBy(x => x.VoziloID).OrderByDescending(y => y.Count())
                .Select(y => new Models.ZakupVozilo { ID = y.Key.Value, 
                    GodinaProizvodnje = y.Select(z => z.Vozilo.GodinaProizvodnje).FirstOrDefault(),
                    ModelVozila = y.Select(z => z.Vozilo.ModelVozila.Naziv).FirstOrDefault(),
                    Proizvodjac = y.Select(z => z.Vozilo.ModelVozila.Proizvodjac.Naziv).FirstOrDefault(),
                    RegistarskiBroj = y.Select(z => z.Vozilo.RegistarskiBroj).FirstOrDefault(),
                    Slika =  y.Select(z => z.Vozilo.Slika).FirstOrDefault()}).Take(Broj);
        }

        /// <summary>
        /// Dohvaća vozilo prema njegovoj registraciji.
        /// </summary>
        /// <param name="Registracija"></param>
        /// <returns></returns>
        public Vozilo DohvatiVoziloPoRegistraciji(string Registracija)
        {
            return db.Voziloes.FirstOrDefault(x => x.RegistarskiBroj.Equals(Registracija.ToUpper()));
        }

        /// <summary>
        /// Čuva vozilo.
        /// </summary>
        /// <param name="vozilo"></param>
        public void Sacuvaj(Vozilo vozilo)
        {
            db.Voziloes.Add(vozilo);
            db.SaveChanges();
        }

        /// <summary>
        /// Briše vozilo.
        /// </summary>
        /// <param name="vozilo"></param>
        public void Obrisi(Vozilo vozilo)
        {
            db.Voziloes.Remove(vozilo);
            db.SaveChanges();
        }

        /// <summary>
        /// Ažurira vozilo.
        /// </summary>
        /// <param name="vozilo"></param>
        public void Azuriraj(Vozilo vozilo)
        {
            var model = db.Voziloes.FirstOrDefault(x => x.ID == vozilo.ID);

            model.Boja = vozilo.Boja;
            model.CijenaPoDanu = vozilo.CijenaPoDanu;
            model.FilijalaID = vozilo.FilijalaID;
            model.GodinaProizvodnje = vozilo.GodinaProizvodnje;
            model.ModelVozilaID = vozilo.ModelVozilaID;
            model.RegistarskiBroj = vozilo.RegistarskiBroj;
            model.Slika = vozilo.Slika;

            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}