﻿using RentACar.App_Start;
using RentACar.BLL;
using RentACar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RentACar.Controllers
{
    public class GradController : Controller
    {
        private GradServis gradServis;

        public GradController()
        {
            this.gradServis = new GradServis();
        }

        /// <summary>
        /// Prikazuje listu gradova.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        public ActionResult Index()
        {
            var gradovi = gradServis.DohvatiGradove();
            return View(gradovi);
        }

        /// <summary>
        /// Prikazuje formu za kreiranje novog grada.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult KreirajGrad()
        {
            return View();
        }

        /// <summary>
        /// Kreira novi grad.
        /// </summary>
        /// <param name="isModal"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult KreirajGrad(bool? isModal, Grad model)
        {
            if (ModelState.IsValid)
            {
                var grad = new DAL.Grad
                {
                    Naziv = model.Naziv,
                    PozivniBroj = model.PozivniBroj,
                    ID = Guid.NewGuid()
                };

                if(gradServis.DohvatiGradPoNazivu(grad.Naziv) != null)
                {
                    ModelState.AddModelError("", "Unešeni grad već postoji.");
                    return View(model);
                }
                else
                {
                    gradServis.Sacuvaj(grad);
                    if (isModal.HasValue)
                        return Json("OK");
                    else
                        return RedirectToAction("Index");
                }
                
            }
            return View(model);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg grada.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult UrediGrad(Guid ID)
        {
            var proizvodjac = gradServis.DohvatiGrad(ID);
            var model = new Grad
            {
                Naziv = proizvodjac.Naziv,
                PozivniBroj = proizvodjac.PozivniBroj,
                ID = proizvodjac.ID
            };
            return View(model);
        }

        /// <summary>
        /// Ažurira postojeći grad.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult UrediGrad(Grad model)
        {
            if (ModelState.IsValid)
            {
                var grad = new DAL.Grad
                {
                    PozivniBroj = model.PozivniBroj,
                    ID = model.ID
                };

                gradServis.Azuriraj(grad);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// Briše grad prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        public JsonResult ObrisiGrad(Guid ID)
        {
            var grad = gradServis.DohvatiGrad(ID);
            gradServis.Obrisi(grad);

            return Json("OK");
        }
    }
}