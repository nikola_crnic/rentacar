﻿using RentACar.App_Start;
using RentACar.BLL;
using RentACar.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RentACar.Controllers
{
    public class ProizvodjacController : Controller
    {
        private ProizvodjacServis proizvodjacServis;

        public ProizvodjacController()
        {
            this.proizvodjacServis = new ProizvodjacServis();
        }

        /// <summary>
        /// Prikazuje listu proizvođača.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        public ActionResult Index()
        {
            var proizvodjaci = proizvodjacServis.DohvatiProizvodjace();
            return View(proizvodjaci);
        }

        /// <summary>
        /// Prikazuje formu za unos novog proizvođača.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult KreirajProizvodjaca()
        {
            return View();
        }

        /// <summary>
        /// Kreira novog proizvođača.
        /// </summary>
        /// <param name="isModal"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult KreirajProizvodjaca(bool? isModal, Proizvodjac model)
        {
            if (ModelState.IsValid)
            {
                var proizvodjac = new DAL.Proizvodjac
                {
                    Naziv = model.Naziv,
                    Logo = model.Logo,
                    ID = Guid.NewGuid()
                };

                if(proizvodjacServis.DohvatiProizvodjacaPoNazivu(proizvodjac.Naziv) != null)
                {
                    ModelState.AddModelError("", "Unešeni proizvođač već postoji");
                    return View(model);
                }
                else
                {
                    proizvodjacServis.Sacuvaj(proizvodjac);

                    if (isModal.HasValue && isModal.Value)
                        return Json("OK");
                    else
                        return RedirectToAction("Index", "Vozilo");
                }
                
            }
            return View(model);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg proizvođača.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult UrediProizvodjaca(Guid ID)
        {
            var proizvodjac = proizvodjacServis.DohvatiProizvodjaca(ID);
            var model = new Proizvodjac
            {
                Naziv = proizvodjac.Naziv,
                Logo = proizvodjac.Logo,
                ID = proizvodjac.ID
            };
            return View(model);
        }

        /// <summary>
        /// Ažurira postojećeg proizvođača.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult UrediProizvodjaca(Proizvodjac model)
        {
            if (ModelState.IsValid)
            {
                if (proizvodjacServis.DohvatiProizvodjaca(model.ID).Logo != null && model.Logo != null)
                    ObrisiLogo(proizvodjacServis.DohvatiProizvodjaca(model.ID).Logo);

                var proizvodjac = new DAL.Proizvodjac
                {
                    Logo = model.Logo,
                    ID = model.ID
                };

                proizvodjacServis.Azuriraj(proizvodjac);
                return RedirectToAction("Index", "Vozilo");
            }
            return View(model);
        }

        /// <summary>
        /// Briše proizvođača prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        public JsonResult ObrisiProizvodjaca(Guid ID)
        {
            var proizvodjac = proizvodjacServis.DohvatiProizvodjaca(ID);
            proizvodjacServis.Obrisi(proizvodjac);

            if (proizvodjac.Logo != null)
                ObrisiLogo(proizvodjac.Logo);

            return Json("OK");
        }

        /// <summary>
        /// Čuva logo za traženog proizvođača.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public JsonResult SacuvajSliku()
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var fileName = Path.GetFileName(file.FileName);
                var path = "/Slike/Proizvodjaci/" + fileName;
                var fullPath = Server.MapPath(path);
                file.SaveAs(fullPath);

                return Json(path, JsonRequestBehavior.AllowGet);
            }
            return Json("ERROR");
        }

        /// <summary>
        /// Briše logo za traženog proizvođača.
        /// </summary>
        /// <param name="Logo"></param>
        [Autorizacija]
        [HttpPost]
        public void ObrisiLogo(string Logo)
        {
            string fullPath = Request.MapPath(Logo);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
        }
    }
}