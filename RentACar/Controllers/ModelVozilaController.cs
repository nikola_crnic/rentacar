﻿using RentACar.App_Start;
using RentACar.BLL;
using RentACar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RentACar.Controllers
{
    public class ModelVozilaController : Controller
    {
        private ModelVozilaServis modelVozilaServis;

        public ModelVozilaController()
        {
            this.modelVozilaServis = new ModelVozilaServis();
        }

        /// <summary>
        /// Prikazuje listu modela vozila.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        public ActionResult Index()
        {
            var modeliVozila = modelVozilaServis.DohvatiModeleVozila();
            return View(modeliVozila);
        }

        /// <summary>
        /// Prikazuje formu za unos novog modela vozila.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult KreirajModelVozila()
        {
            var proizvodjaci = modelVozilaServis.DohvatiProizvodjace();
            var tipoviVozila = modelVozilaServis.DohvatiTipoveVozila();
            var tipoviGoriva = modelVozilaServis.DohvatiTipoveGoriva();

            ViewBag.Proizvodjaci = proizvodjaci.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });

            ViewBag.TipoviVozila = tipoviVozila.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });

            ViewBag.TipoviGoriva = tipoviGoriva.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        }); 
            return View();
        }

        /// <summary>
        /// Kreira novi model vozila.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult KreirajModelVozila(ModelVozila model)
        {
            if (ModelState.IsValid)
            {
                var modelVozila = new DAL.ModelVozila
                {
                    Naziv = model.Naziv,
                    Automatik = model.Automatik,
                    ProizvodjacID = model.ProizvodjacID,
                    TipGorivaID = model.TipGorivaID,
                    TipVozilaID = model.TipVozilaID,
                    ID = Guid.NewGuid()
                };

                if(modelVozilaServis.DohvatiModelVozilaPoNazivu(modelVozila.Naziv) != null)
                {
                    ModelState.AddModelError("", "Unešeni model vozila već postoji.");
                }
                else
                {
                    modelVozilaServis.Sacuvaj(modelVozila);
                    return RedirectToAction("Index", "Vozilo");
                }
                
            }

            var proizvodjaci = modelVozilaServis.DohvatiProizvodjace();
            var tipoviVozila = modelVozilaServis.DohvatiTipoveVozila();
            var tipoviGoriva = modelVozilaServis.DohvatiTipoveGoriva();

            ViewBag.Proizvodjaci = proizvodjaci.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });

            ViewBag.TipoviVozila = tipoviVozila.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });

            ViewBag.TipoviGoriva = tipoviGoriva.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        }); 

            return View(model);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg vozila.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult UrediModelVozila(Guid ID)
        {
            var proizvodjaci = modelVozilaServis.DohvatiProizvodjace();
            var tipoviVozila = modelVozilaServis.DohvatiTipoveVozila();
            var tipoviGoriva = modelVozilaServis.DohvatiTipoveGoriva();

            ViewBag.Proizvodjaci = proizvodjaci.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });

            ViewBag.TipoviVozila = tipoviVozila.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });

            ViewBag.TipoviGoriva = tipoviGoriva.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        }); 

            var modelVozila = modelVozilaServis.DohvatiModelVozila(ID);
            var model = new ModelVozila
            {
                Naziv = modelVozila.Naziv,
                Automatik = modelVozila.Automatik,
                ProizvodjacID = modelVozila.ProizvodjacID,
                TipGorivaID = modelVozila.TipGorivaID,
                TipVozilaID = modelVozila.TipVozilaID,
                BrojOsoba = modelVozila.BrojOsoba,
                ID = modelVozila.ID
            };
            return View(model);
        }

        /// <summary>
        /// Ažurira postojeći model vozila.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult UrediModelVozila(ModelVozila model)
        {
            if (ModelState.IsValid)
            {
                var modelVozila = new DAL.ModelVozila
                {
                    Naziv = model.Naziv,
                    Automatik = model.Automatik,
                    ProizvodjacID = model.ProizvodjacID,
                    TipGorivaID = model.TipGorivaID,
                    TipVozilaID = model.TipVozilaID,
                    BrojOsoba = model.BrojOsoba,
                    ID = model.ID
                };

                modelVozilaServis.Azuriraj(modelVozila);
                return RedirectToAction("Index", "Vozilo");
            }
            return View(model);
        }

        /// <summary>
        /// Briše model vozila prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        public JsonResult ObrisiModelVozila(Guid ID)
        {
            var modelVozila = modelVozilaServis.DohvatiModelVozila(ID);
            modelVozilaServis.Obrisi(modelVozila);

            return Json("OK");
        }
    }
}