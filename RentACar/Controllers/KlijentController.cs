﻿using RentACar.App_Start;
using RentACar.BLL;
using RentACar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RentACar.Controllers
{
    public class KlijentController : Controller
    {
        private KlijentServis klijentServis;

        public KlijentController()
        {
            this.klijentServis = new KlijentServis();
        }

        /// <summary>
        /// Prikazuje listu klijenata.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        public ActionResult Index()
        {
            var klijenti = klijentServis.DohvatiKlijente();
            return View(klijenti);
        }

        /// <summary>
        /// Prikazuje formu za kreiranje novog klijenta.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult KreirajKlijenta()
        {
            var model = new Klijent();
            return View(model);
        }

        /// <summary>
        /// Kreira novog klijenta.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult KreirajKlijenta(Klijent model)
        {
            if (ModelState.IsValid)
            {
                var klijent = new DAL.Klijent
                {
                    Ime = model.Ime,
                    Prezime = model.Prezime,
                    JMBG = model.JMBG,
                    Email = model.Email,
                    BrojVD = model.BrojVD,
                    BrojLK = model.BrojLK,
                    BrojTelefona = model.BrojTelefona,
                    ID = model.ID
                };

                if (klijentServis.DohvatiKlijentaPoEmailu(model.Email) != null)
                {
                    return Json("ERROR_POSTOJI");
                }
                else
                {
                    klijentServis.Sacuvaj(klijent);
                    return Json("OK");
                }
            }
            return Json("ERROR");
        }

        /// <summary>
        /// Prikazuje formu za uređivanje klijentskog naloga.
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UrediMojNalog(string Email)
        {
            var klijent = klijentServis.DohvatiKlijentaPoEmailu(Email);
            ViewBag.Zakupi = klijentServis.DohvatiZakupePoKlijentu(klijent.ID);

            var model = new Klijent
            {
                Ime = klijent.Ime,
                Prezime = klijent.Prezime,
                JMBG = klijent.JMBG,
                Email = klijent.Email,
                BrojVD = klijent.BrojVD,
                BrojLK = klijent.BrojLK,
                BrojTelefona = klijent.BrojTelefona,
                ID = klijent.ID
            };

            return View(model);

        }

        /// <summary>
        /// Ažurira klijentski nalog.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UrediMojNalog(Klijent model)
        {
            if (ModelState.IsValid)
            {
                var klijent = new DAL.Klijent
                {
                    Ime = model.Ime,
                    Prezime = model.Prezime,
                    JMBG = model.JMBG,
                    BrojVD = model.BrojVD,
                    BrojLK = model.BrojLK,
                    BrojTelefona = model.BrojTelefona,
                    ID = model.ID
                };

                klijentServis.Azuriraj(klijent);
                return Redirect("/Home/Index");
            }
            return View();
        }

        /// <summary>
        /// Ažurira postojećeg klijenta.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UrediKlijenta(Klijent model)
        {
            if (ModelState.IsValid)
            {
                var klijent = new DAL.Klijent
                {
                    Ime = model.Ime,
                    Prezime = model.Prezime,
                    JMBG = model.JMBG,
                    BrojVD = model.BrojVD,
                    BrojLK = model.BrojLK,
                    BrojTelefona = model.BrojTelefona,
                    ID = model.ID
                };

                klijentServis.Azuriraj(klijent);
                return Json("OK");
            }
            return Json("ERROR");
        }

        /// <summary>
        /// Prikazuje detalje traženog klijenta.
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DetaljiKlijenta(string Email)
        {
            var klijent = klijentServis.DohvatiKlijentaPoEmailu(Email);

            if(klijent != null)
            {
                var model = new Klijent
                {
                    Ime = klijent.Ime,
                    Prezime = klijent.Prezime,
                    JMBG = klijent.JMBG,
                    Email = klijent.Email,
                    BrojVD = klijent.BrojVD,
                    BrojLK = klijent.BrojLK,
                    BrojTelefona = klijent.BrojTelefona,
                    ID = klijent.ID
                };
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("ERROR");
        }

        /// <summary>
        /// Briše klijenta prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        public JsonResult ObrisiKlijenta(Guid ID)
        {
            var klijent = klijentServis.DohvatiKlijenta(ID);
            klijentServis.Obrisi(klijent);

            return Json("OK");
        }
    }
}