﻿using RentACar.App_Start;
using RentACar.BLL;
using RentACar.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RentACar.Controllers
{
    public class VoziloController : Controller
    {
        private VoziloServis voziloServis;

        public VoziloController()
        {
            this.voziloServis = new VoziloServis();
        }

        /// <summary>
        /// Prikazuje vozila, proizvođače i modele vozila.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Prikazuje listu vozila.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        public ActionResult ListaVozila()
        {
            var vozila = voziloServis.DohvatiVozila();
            return View(vozila);
        }

        /// <summary>
        /// Prikazuje formu za unos novog vozila.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult KreirajVozilo()
        {
            var modeliVozila = voziloServis.DohvatiModeleVozila();
            var filijale = voziloServis.DohvatiFilijale();
            ViewBag.ModeliVozila = modeliVozila.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });

            ViewBag.Filijale = filijale.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        }); 
            return View();
        }

        /// <summary>
        /// Kreira novo vozilo.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult KreirajVozilo(Vozilo model)
        {
            if (ModelState.IsValid)
            {
                var vozilo = new DAL.Vozilo
                {
                    Boja = model.Boja,
                    RegistarskiBroj = model.RegistarskiBroj.ToUpper(),
                    CijenaPoDanu = model.CijenaPoDanu,
                    GodinaProizvodnje = model.GodinaProizvodnje,
                    FilijalaID = model.FilijalaID,
                    ModelVozilaID = model.ModelVozilaID,
                    Slika = model.Slika,
                    ID = Guid.NewGuid()
                };
                if (voziloServis.DohvatiVoziloPoRegistraciji(vozilo.RegistarskiBroj) != null)
                {
                    ModelState.AddModelError("", "Vozilo sa registarskim brojem " + vozilo.RegistarskiBroj + " već postoji.");
                } 
                else
                {
                    voziloServis.Sacuvaj(vozilo);
                    return RedirectToAction("Index");
                }
            }

            var modeliVozila = voziloServis.DohvatiModeleVozila();
            var filijale = voziloServis.DohvatiFilijale();
            ViewBag.ModeliVozila = modeliVozila.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });

            ViewBag.Filijale = filijale.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        }); 

            return View(model);
        }

        /// <summary>
        /// Prikazuje detalje traženog vozila.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DetaljiVozila(Guid ID)
        {
            var vozilo = voziloServis.DohvatiVozilo(ID);

            var model = new Vozilo
            {
                Boja = vozilo.Boja,
                CijenaPoDanu = vozilo.CijenaPoDanu,
                GodinaProizvodnje = vozilo.GodinaProizvodnje,
                BrojOsoba = vozilo.ModelVozila.BrojOsoba,
                TipGoriva = vozilo.ModelVozila.TipGoriva.Naziv,
                TipVozila = vozilo.ModelVozila.TipVozila.Naziv,
                Automatik = vozilo.ModelVozila.Automatik,
                ID = vozilo.ID,
                RegistarskiBroj = vozilo.RegistarskiBroj,
                Slika = vozilo.Slika
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg vozila.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult UrediVozilo(Guid ID)
        {
            var modeliVozila = voziloServis.DohvatiModeleVozila();
            var filijale = voziloServis.DohvatiFilijale();
            ViewBag.ModeliVozila = modeliVozila.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });

            ViewBag.Filijale = filijale.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        }); 

            var vozilo = voziloServis.DohvatiVozilo(ID);
            var model = new Vozilo
            {
                Boja = vozilo.Boja,
                CijenaPoDanu = vozilo.CijenaPoDanu,
                FilijalaID = vozilo.FilijalaID,
                GodinaProizvodnje = vozilo.GodinaProizvodnje,
                ModelVozilaID = vozilo.ModelVozilaID,
                RegistarskiBroj = vozilo.RegistarskiBroj,
                Slika = vozilo.Slika,
                ID = vozilo.ID
            };
            return View(model);
        }

        /// <summary>
        /// Ažurira postojeće vozilo.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult UrediVozilo(Vozilo model)
        {
            if (ModelState.IsValid)
            {
                if (voziloServis.DohvatiVozilo(model.ID).Slika != null && model.Slika != null)
                    ObrisiSliku(voziloServis.DohvatiVozilo(model.ID).Slika);

                var vozilo = new DAL.Vozilo
                {
                    Boja = model.Boja,
                    RegistarskiBroj = model.RegistarskiBroj.ToUpper(),
                    CijenaPoDanu = model.CijenaPoDanu,
                    GodinaProizvodnje = model.GodinaProizvodnje,
                    FilijalaID = model.FilijalaID,
                    ModelVozilaID = model.ModelVozilaID,
                    Slika = model.Slika,
                    ID = model.ID
                };

                voziloServis.Azuriraj(vozilo);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// Briše vozilo prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        public JsonResult ObrisiVozilo(Guid ID)
        {
            var vozilo = voziloServis.DohvatiVozilo(ID);

            voziloServis.Obrisi(vozilo);

            if (vozilo.Slika != null)
                ObrisiSliku(vozilo.Slika);

            return Json("OK");
        }

        /// <summary>
        /// Čuva sliku za traženo vozilo.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public JsonResult SacuvajSliku()
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];

                var fileName = Path.GetFileName(file.FileName);
                var path = "/Slike/Vozila/" + fileName;
                var fullPath = Server.MapPath(path);
                file.SaveAs(fullPath);

                return Json(path, JsonRequestBehavior.AllowGet);
            }
            return Json("ERROR");
        }

        /// <summary>
        /// Briše sliku za traženo vozilo.
        /// </summary>
        /// <param name="Slika"></param>
        [Autorizacija]
        [HttpPost]
        public void ObrisiSliku(string Slika)
        {
            string fullPath = Request.MapPath(Slika);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
        }

        /// <summary>
        /// Lista najčešće iznajmljivanih vozila.
        /// </summary>
        /// <param name="Broj"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public JsonResult TopVozila(int Broj)
        {
            var vozila = voziloServis.DohvatiTopVozila(Broj);
            return Json(vozila, JsonRequestBehavior.AllowGet);
        }
    }
}