﻿using RentACar.App_Start;
using RentACar.BLL;
using RentACar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace RentACar.Controllers
{
    public class FilijalaController : Controller
    {
        private FilijalaServis filijalaServis;

        public FilijalaController()
        {
            this.filijalaServis = new FilijalaServis();
        }

        /// <summary>
        /// Prikazuje listu filijala.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        public ActionResult Index()
        {
            var filijale = filijalaServis.DohvatiFilijale();
            return View(filijale);
        }

        /// <summary>
        /// Prikazuje formu za unos nove filijale.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult KreirajFilijalu()
        {
            var gradovi = filijalaServis.DohvatiGradove();
            ViewBag.Gradovi = gradovi.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        }); 
            return View();
        }

        /// <summary>
        /// Kreira novu filijalu.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult KreirajFilijalu(Filijala model)
        {
            if (ModelState.IsValid)
            {
                var filijala = new DAL.Filijala
                {
                    Adresa = model.Adresa,
                    BrojTelefona = model.BrojTelefona,
                    GradID = model.GradID,
                    Naziv = model.Naziv,
                    ID = Guid.NewGuid()
                };

                var postojecaFilijala = filijalaServis.DohvatiFilijaluPoNazivu(filijala.Naziv);

                if(postojecaFilijala != null)
                {
                    ModelState.AddModelError("", "Filijala sa unešenim nazivom već postoji.");
                }
                else
                {
                    filijalaServis.Sacuvaj(filijala);
                    return RedirectToAction("Index");
                }
            }

            var gradovi = filijalaServis.DohvatiGradove();
            ViewBag.Gradovi = gradovi.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        }); 

            return View(model);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje filijale.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult UrediFilijalu(Guid ID)
        {
            var gradovi = filijalaServis.DohvatiGradove();
            ViewBag.Gradovi = gradovi.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        }); 

            var filijala = filijalaServis.DohvatiFilijalu(ID);
            var model = new Filijala
            {
                Adresa = filijala.Adresa,
                GradID = filijala.GradID,
                Naziv = filijala.Naziv,
                BrojTelefona = filijala.BrojTelefona,
                ID = filijala.ID
            };
            return View(model);
        }

        /// <summary>
        /// Ažurira postojeću filijalu.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpPost]
        public ActionResult UrediFilijalu(Filijala model)
        {
            if (ModelState.IsValid)
            {
                var filijala = new DAL.Filijala
                {
                    Adresa = model.Adresa,
                    GradID = model.GradID,
                    BrojTelefona = model.BrojTelefona,
                    ID = model.ID
                };

                filijalaServis.Azuriraj(filijala);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// Briše filijalu prema njenom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        public JsonResult ObrisiFilijalu(Guid ID)
        {
            var filijala = filijalaServis.DohvatiFilijalu(ID);
            filijalaServis.Obrisi(filijala);

            return Json("OK");
        }

        /// <summary>
        /// Prikazuje grad prema njegovom ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public JsonResult DohvatiGrad(Guid ID)
        {
            var model = filijalaServis.DohvatiGrad(ID);
            var grad = new Grad
            {
                ID = model.ID,
                Naziv = model.Naziv,
                PozivniBroj = model.PozivniBroj
            };

            return Json(grad, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Prikazuje promet prema filijalama.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public JsonResult PrometPoFilijalama()
        {
            var filijale = filijalaServis.DohvatiPrometPoFilijalama();
            return Json(filijale, JsonRequestBehavior.AllowGet);
        }
    }
}