﻿using RentACar.BLL;
using RentACar.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace RentACar.Controllers
{
    public class KorisnikController : Controller
    {
        private KorisnikServis korisnikServis;

        public KorisnikController()
        {
            this.korisnikServis = new KorisnikServis();
        }

        
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Prikazuje formu za prijavu.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Prijava()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Dashboard", "Home");
            return View();
        }

        /// <summary>
        /// Prijavljuje korisnika na sistem.
        /// </summary>
        /// <param name="korisnik"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Prijava(Korisnik korisnik)
        {
            if(ModelState.IsValid)
            {
                if (IsValid(korisnik.Email, korisnik.Lozinka))
                {
                    FormsAuthentication.SetAuthCookie(korisnik.Email, false);
                    return RedirectToAction("Dashboard", "Home");
                }
                else
                    ModelState.AddModelError("", "Pogrešan email ili lozinka.");
            }
            return View(korisnik);
        }

        //public void Registracija()
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var crypto = new SimpleCrypto.PBKDF2();
        //        var cryptoLozinka = crypto.Compute("P@ssw0rd");

        //        var model = new DAL.Korisnik
        //        {
        //            Email = "crnic.nikola@gmail.com",
        //            Lozinka = cryptoLozinka,
        //            LozinkaSalt = crypto.Salt,
        //            KorisnikID = Guid.NewGuid()
        //        };

        //        korisnikServis.Sacuvaj(model);
        //    }
        //}

        /// <summary>
        /// Odjavljuje korisnika sa sistema.
        /// </summary>
        /// <returns></returns>
        public ActionResult Odjava()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Provjerava da li su unešeni podaci za prijavu na sistem ispravni.
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="Lozinka"></param>
        /// <returns></returns>
        public bool IsValid(string Email, string Lozinka)
        {
            bool isValid = false;

            var crypto = new SimpleCrypto.PBKDF2();

            var korisnik = korisnikServis.DohvatiKorisnika(Email);

            if(korisnik != null)
            {
                if (korisnik.Lozinka == crypto.Compute(Lozinka, korisnik.LozinkaSalt))
                    isValid = true;
            }

            return isValid;
        }
    }
}