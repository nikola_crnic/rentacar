﻿using RentACar.App_Start;
using RentACar.BLL;
using RentACar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RentACar.Controllers
{
    public class HomeController : Controller
    {
        private ZakupServis zakupServis;

        public HomeController()
        {
            this.zakupServis = new ZakupServis();
        }

        /// <summary>
        /// Prikazuje listu gradova.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var gradovi = zakupServis.DohvatiGradove();
            ViewBag.Gradovi = gradovi.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });
            return View();
        }

        /// <summary>
        /// Prikazuje stranicu o nama.
        /// </summary>
        /// <returns></returns>
        public ActionResult Onama()
        {   
            return View();
        }

        /// <summary>
        /// Prikazuje kontakt stranicu.
        /// </summary>
        /// <returns></returns>
        public ActionResult Kontakt()
        {
            ViewBag.Filijale = zakupServis.DohvatiFilijale();
            return View();
        }

        /// <summary>
        /// Prikazuje kontrolnu tablu.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public ActionResult Dashboard()
        {
            return View();
        }

        /// <summary>
        /// Statistički prikaz zakupa po mjesecima.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        [HttpGet]
        public JsonResult ZakupiStatistika()
        {
            var zakupi = zakupServis.DohvatiZakupePoMjesecima();
            return Json(zakupi, JsonRequestBehavior.AllowGet);
        }
    }
}