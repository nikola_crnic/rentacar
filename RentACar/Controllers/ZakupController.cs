﻿using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using RentACar.App_Start;
using RentACar.BLL;
using RentACar.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security;
using System.Security.Permissions;
using System.Web;
using System.Web.Mvc;

namespace RentACar.Controllers
{
    public class ZakupController : Controller
    {
        private ZakupServis zakupServis;

        public ZakupController()
        {
            this.zakupServis = new ZakupServis();
        }

        /// <summary>
        /// Prikazuje listu zakupa.
        /// </summary>
        /// <returns></returns>
        [Autorizacija]
        public ActionResult Index()
        {
            var zakupi = zakupServis.DohvatiZakupe();
            return View(zakupi);
        }

        /// <summary>
        /// Prikazuje formu za unos novog zakupa.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult KreirajZakup()
        {
            var gradovi = zakupServis.DohvatiGradove();
            ViewBag.Gradovi = gradovi.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });
            return View();
        }

        /// <summary>
        /// Kreira novi zakup.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult KreirajZakup(Zakup model)
        {
            if (ModelState.IsValid)
            {
                var zakup = new DAL.Zakup
                {
                    DatumPovratka = model.DatumPovratka,
                    DatumZakupa = model.DatumZakupa,
                    FilijalaID = model.FilijalaID,
                    Komentar = model.Komentar,
                    KlijentID = model.KlijentID,
                    VoziloID = model.VoziloID,
                    StatusID = model.StatusID,
                    Ukupno = model.Ukupno,
                    SifraZakupa = model.SifraZakupa,
                    ID = model.ID
                };

                zakupServis.Sacuvaj(zakup);

                if(zakup.StatusID == 2 && zakup.KlijentID.HasValue)
                {
                    var klijent = zakupServis.DohvatiKlijenta(zakup.KlijentID.Value);
                    var filijala = zakupServis.DohvatiFilijalu(zakup.FilijalaID.Value);
                    var vozilo = zakupServis.DohvatiVozilo(zakup.VoziloID.Value);

                    var faktura = new Faktura
                    {
                        BrojLK = klijent.BrojLK,
                        BrojVD = klijent.BrojVD,
                        DatumPovratka = zakup.DatumPovratka,
                        DatumZakupa = zakup.DatumZakupa,
                        Email = klijent.Email,
                        FilijalaAdresa = filijala.Adresa,
                        FilijalaNaziv = filijala.Naziv,
                        FilijalaTelefon = filijala.BrojTelefona,
                        GodinaProizvodnje = vozilo.GodinaProizvodnje,
                        GradNaziv = filijala.Grad.Naziv,
                        Ime = klijent.Ime,
                        JMBG = klijent.JMBG,
                        KlijentTelefon = klijent.BrojTelefona,
                        ModelNaziv = vozilo.ModelVozila.Naziv,
                        Prezime = klijent.Prezime,
                        ProizvodjacNaziv = vozilo.ModelVozila.Proizvodjac.Naziv,
                        RegistarskiBroj = vozilo.RegistarskiBroj,
                        SifraZakupa = zakup.SifraZakupa,
                        Ukupno = zakup.Ukupno,
                        CijenaPoDanu = vozilo.CijenaPoDanu
                    };

                    var pdf = KreirajFakturu(faktura);

                    var email = new Email
                    {
                        To = zakupServis.DohvatiKlijenta(zakup.KlijentID.Value).Email,
                        Body = "Faktura za rezervaciju: #" + zakup.SifraZakupa,
                        Subject = "Potvrda rezervacije"
                    };

                    PosaljiEmail(email, pdf.FileContents);
                }

                return Json("OK");
            }

            return Json("ERROR");
        }

        /// <summary>
        /// Prikazuje detalje traženog zakupa.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DetaljiZakupa(Guid ID)
        {
            var zakup = zakupServis.DohvatiZakup(ID);

            var model = new Zakup
            {
                DatumPovratka = zakup.DatumPovratka,
                DatumZakupa = zakup.DatumZakupa,
                FilijalaID = zakup.FilijalaID,
                KlijentID = zakup.KlijentID,
                VoziloID = zakup.VoziloID,
                Komentar = zakup.Komentar,
                ID = zakup.ID
            };

            return View(model);
        }

        /// <summary>
        /// Prikazuje formu za ažuriranje postojećeg zakupa.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UrediZakup(Guid ID)
        {
            var gradovi = zakupServis.DohvatiGradove();
            ViewBag.Gradovi = gradovi.Select(x =>
                        new SelectListItem
                        {
                            Value = x.ID.ToString(),
                            Text = x.Naziv
                        });
            var zakup = zakupServis.DohvatiZakup(ID);

            var model = new Zakup
            {
                DatumPovratka = zakup.DatumPovratka,
                DatumZakupa = zakup.DatumZakupa,
                FilijalaID = zakup.FilijalaID,
                ID = zakup.ID
            };

            return View(model);
        }

        /// <summary>
        /// Ažurira postojeći zakup.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UrediZakup(Zakup model)
        {
            if (ModelState.IsValid)
            {
                var zakup = new DAL.Zakup
                {
                    KlijentID = model.KlijentID,
                    VoziloID = model.VoziloID,
                    DatumZakupa = model.DatumZakupa,
                    DatumPovratka = model.DatumPovratka,
                    Komentar = model.Komentar,
                    Ukupno = model.Ukupno,
                    ID = model.ID
                };
                zakupServis.Azuriraj(zakup);


                var klijent = zakupServis.DohvatiKlijenta(zakup.KlijentID.Value);
                var filijala = zakupServis.DohvatiFilijalu(model.FilijalaID.Value);
                var vozilo = zakupServis.DohvatiVozilo(zakup.VoziloID.Value);

                var faktura = new Faktura
                {
                    BrojLK = klijent.BrojLK,
                    BrojVD = klijent.BrojVD,
                    DatumPovratka = zakup.DatumPovratka,
                    DatumZakupa = zakup.DatumZakupa,
                    Email = klijent.Email,
                    FilijalaAdresa = filijala.Adresa,
                    FilijalaNaziv = filijala.Naziv,
                    FilijalaTelefon = filijala.BrojTelefona,
                    GodinaProizvodnje = vozilo.GodinaProizvodnje,
                    GradNaziv = filijala.Grad.Naziv,
                    Ime = klijent.Ime,
                    JMBG = klijent.JMBG,
                    KlijentTelefon = klijent.BrojTelefona,
                    ModelNaziv = vozilo.ModelVozila.Naziv,
                    Prezime = klijent.Prezime,
                    ProizvodjacNaziv = vozilo.ModelVozila.Proizvodjac.Naziv,
                    RegistarskiBroj = vozilo.RegistarskiBroj,
                    SifraZakupa = zakupServis.DohvatiZakup(zakup.ID).SifraZakupa,
                    Ukupno = zakup.Ukupno,
                    CijenaPoDanu = vozilo.CijenaPoDanu
                };

                var pdf = KreirajFakturu(faktura);

                var email = new Email
                {
                    To = klijent.Email,
                    Body = "Faktura za rezervaciju: #" + faktura.SifraZakupa,
                    Subject = "Potvrda rezervacije"
                };

                PosaljiEmail(email, pdf.FileContents);


                return Json("OK");
            }
            return Json("ERROR");
        }

        /// <summary>
        /// Briše zakup prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public JsonResult ObrisiZakup(Guid ID)
        {
            var zakup = zakupServis.DohvatiZakup(ID);
            zakupServis.Obrisi(zakup);

            return Json("OK");
        }

        /// <summary>
        /// Lista filijala prema gradu u kom se nalaze.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DohvatiFilijale(Guid ID)
        {
            var filijale = zakupServis.DohvatiFilijalePoGradu(ID);

            var model = filijale.Select(x =>
                        new 
                        {
                            id = x.ID.ToString(),
                            text = x.Naziv
                        });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Lista vozila prema filijali kojoj pripadaju.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DohvatiVozila(Guid ID)
        {
            var vozila = zakupServis.DohvatiVozilaPoFilijali(ID);

            var model = vozila.Select(x =>
                        new 
                        {
                            id = x.ID.ToString(),
                            text = x.ModelVozila.Proizvodjac.Naziv + " " + x.ModelVozila.Naziv
                        });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Lista svih vozila.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult DohvatiSvaVozila()
        {
            var vozila = zakupServis.DohvatiVozila();

            var model = vozila.Select(x =>
                        new
                        {
                            id = x.ID.ToString(),
                            text = x.ModelVozila.Proizvodjac.Naziv + " " + x.ModelVozila.Naziv
                        });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Mijenja status zakupa prema ID-u.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public JsonResult PromjeniStatus(Guid ID)
        {
            zakupServis.PromjeniStatus(ID);

            return Json("OK");
        }

        /// <summary>
        /// Provjerava dostupnost vozila.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="DatumOd"></param>
        /// <param name="DatumDo"></param>
        /// <returns></returns>
        public JsonResult ProvjeriDostupnostVozila(Guid ID, DateTime DatumOd, DateTime DatumDo)
        {
            var isDostupno = zakupServis.DostupnostVozila(ID, DatumOd, DatumDo);

            if (isDostupno)
                return Json("OK", JsonRequestBehavior.AllowGet);
            else
                return Json("ERROR", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Šalje email klijentu sa fakturom zakupa.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="file"></param>
        [HttpPost]
        public void PosaljiEmail(Email email, byte[] file)
        {
            string from = "rentacarnc@gmail.com"; //example:- name@gmail.com
            using (MailMessage mail = new MailMessage(from, email.To))
            {
                mail.Subject = email.Subject;
                mail.Body = email.Body;
                if (file != null)
                {
                    Stream stream = new MemoryStream(file);
                    string fileName = "faktura.pdf";
                    mail.Attachments.Add(new Attachment(stream, fileName));
                }
                mail.CC.Add(from);
                mail.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential networkCredential = new NetworkCredential(from, "rentacarnc1");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = networkCredential;
                smtp.Port = 587;
                smtp.Send(mail);
                ViewBag.Message = "Sent";
            }
        }

        /// <summary>
        /// Lista zakupa koji imaju status "u procesu".
        /// </summary>
        /// <returns></returns>
        public JsonResult ZakupiUProcesu()
        {
            var zakupi = zakupServis.DohvatiZakupeUProcesu()
                .Select(x => new Zakup { 
                    DatumPovratka = x.DatumPovratka,
                    DatumZakupa = x.DatumZakupa,
                    Ukupno = x.Ukupno.Value,
                    SifraZakupa = x.SifraZakupa,
                    ID = x.ID
                });

            return Json(zakupi, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Generiše fakturu zakupa u pdf formatu.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public FileContentResult KreirajFakturu(Faktura model)
        {
            //LocalReport localReport = new LocalReport();
            //localReport.ReportPath = Server.MapPath("~/Izvjestaji/Faktura.rdlc");
            var reportViewer = new ReportViewer();
            reportViewer.LocalReport.ReportPath = Server.MapPath("~/Izvjestaji/Faktura.rdlc");

            var faktura = new List<Faktura>();
            faktura.Add(model);

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "FakturaDataSet";
            reportDataSource.Value = faktura;
            reportViewer.LocalReport.DataSources.Add(reportDataSource);

            var rp = new Microsoft.Reporting.WebForms.ReportParameter("Sifra", model.SifraZakupa);
            reportViewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter[] { rp });
            reportViewer.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;
            string[] streams;
            Microsoft.Reporting.WebForms.Warning[] warnings;
            string deviceInfo = "<DeviceInfo><OutputFormat>EMF</OutputFormat></DeviceInfo>";

            var renderedBytes = reportViewer.LocalReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension,
                out streams, out warnings);

            return File(renderedBytes, "pdf");
        }
    }
}