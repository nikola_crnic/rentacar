﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class Filijala
    {
        public Guid ID { get; set; }
        public string Naziv { get; set; }
        public string Adresa { get; set; }
        public Guid GradID { get; set; }
        public string BrojTelefona { get; set; }
    }
}