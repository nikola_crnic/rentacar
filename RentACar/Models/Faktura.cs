﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class Faktura
    {
        public System.DateTime? DatumZakupa { get; set; }
        public System.DateTime? DatumPovratka { get; set; }
        public decimal? Ukupno { get; set; }
        public decimal CijenaPoDanu { get; set; }
        public string SifraZakupa { get; set; }

        public string RegistarskiBroj { get; set; }
        public string ModelNaziv { get; set; }
        public string ProizvodjacNaziv { get; set; }
        public int GodinaProizvodnje { get; set; }

        public string Email { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string BrojLK { get; set; }
        public string BrojVD { get; set; }
        public string JMBG { get; set; }
        public string KlijentTelefon { get; set; }

        public string FilijalaNaziv { get; set; }
        public string FilijalaAdresa { get; set; }
        public string FilijalaTelefon { get; set; }
        public string GradNaziv { get; set; }
    }
}