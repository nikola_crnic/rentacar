﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class Zakup
    {
        public Guid ID { get; set; }
        public Guid? KlijentID { get; set; }
        public Guid? FilijalaID { get; set; }
        public Guid? VoziloID { get; set; }
        public int StatusID { get; set; }
        public System.DateTime? DatumZakupa { get; set; }
        public System.DateTime? DatumPovratka { get; set; }
        public string Komentar { get; set; }
        public decimal Ukupno { get; set; }
        public string SifraZakupa { get; set; }
    }
}