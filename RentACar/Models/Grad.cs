﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class Grad
    {
        public Guid ID { get; set; }
        public string Naziv { get; set; }
        public string PozivniBroj { get; set; }
    }
}