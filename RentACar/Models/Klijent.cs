﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class Klijent
    {
        public Guid ID { get; set; }
        public string Email { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string BrojLK { get; set; }
        public string BrojVD { get; set; }
        public string JMBG { get; set; }
        public string BrojTelefona { get; set; }
    }
}