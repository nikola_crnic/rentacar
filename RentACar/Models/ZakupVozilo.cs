﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class ZakupVozilo
    {
        public Guid ID { get; set; }
        public string RegistarskiBroj { get; set; }
        public string ModelVozila { get; set; }
        public string Proizvodjac { get; set; }
        public string Slika { get; set; }
        public int GodinaProizvodnje { get; set; }
    }
}