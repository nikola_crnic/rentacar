﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class ModelVozila
    {
        public Guid ID { get; set; }
        public string Naziv { get; set; }
        public Guid ProizvodjacID { get; set; }
        public bool Automatik { get; set; }
        public int TipVozilaID { get; set; }
        public int TipGorivaID { get; set; }
        public int BrojOsoba { get; set; }
    }
}