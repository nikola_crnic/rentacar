﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class Vozilo
    {
        public Guid ID { get; set; }
        public string RegistarskiBroj { get; set; }
        public string Boja { get; set; }
        public int BrojOsoba { get; set; }
        public bool Automatik { get; set; }
        public string TipGoriva { get; set; }
        public string TipVozila { get; set; }
        public string Slika { get; set; }
        public decimal CijenaPoDanu { get; set; }
        public int GodinaProizvodnje { get; set; }
        public Guid FilijalaID { get; set; }
        public Guid ModelVozilaID { get; set; }
    }
}