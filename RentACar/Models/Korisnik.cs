﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class Korisnik
    {
        public Guid KorisnikID { get; set; }
        public string Email { get; set; }
        public string Lozinka { get; set; }
    }
}