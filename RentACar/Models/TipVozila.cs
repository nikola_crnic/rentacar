﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentACar.Models
{
    public class TipVozila
    {
        public Guid ID { get; set; }
        public string Naziv { get; set; }
    }
}